package com.task1_2;



import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 */
public class FindSecondLengthString {

    /**
     * Method sort array of string by the length from the max to min
     *
     * @param stringArray - array for sorting
     */
    private static void sortArrayByStringLength(final String[] stringArray) {
        if (stringArray == null) {
            return;
        }
        boolean wasReplacement = true;
        while (wasReplacement) {
            wasReplacement = false;
            for (int i = 0; i <= stringArray.length - 2; i++) {
                if (stringArray[i].length() < stringArray[i + 1].length()) {
                    String buffer = stringArray[i];
                    stringArray[i] = stringArray[i + 1];
                    stringArray[i + 1] = buffer;
                    wasReplacement = true;
                }
            }
        }
    }

    /**
     * User enters elements of array
     *
     * @param arraySize - the size of array
     * @return array of string
     */
    private static String[] getStringArray(final int arraySize) {
        BufferedReader inputString = new BufferedReader(new InputStreamReader(System.in));
        String arrayOfString[] = new String[arraySize];
        String userInput = "";
        for (int counter = 0; counter < arraySize; counter++) {
            System.out.println("Enter " + (counter + 1) + " word:");
            try {
                userInput = inputString.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            arrayOfString[counter] = userInput;
        }
        return arrayOfString;
    }

    /**
     * User enter array size
     *
     * @return array size
     */
    private static int getArraySize() {
        BufferedReader InputString = new BufferedReader(new InputStreamReader(System.in));
        int arraySize = 0;
        boolean isInputValid = false;
        while (!isInputValid) {
            System.out.println("Enter the valid number of elements in the array");
            try {
                String userInput = InputString.readLine();
                arraySize = Integer.parseInt(userInput);
                if (arraySize < 2 ) {
                    System.out.println("The size of array can't be less than '2'");
                } else {
                    isInputValid = true;
                }
            } catch (Exception e) {
                System.out.println("You enter invalid size for array. ");
            }
        }
        return arraySize;
    }


    public static void main(String[] args) throws Exception {
        System.out.println("Hello! You run the program that will find the second by length string in array: ");
        BufferedReader InputString = new BufferedReader(new InputStreamReader(System.in));

        boolean isExit = false;
        while (!isExit) {
            int arraySize = getArraySize();
            String[] arrayOfString = getStringArray(arraySize);
            sortArrayByStringLength(arrayOfString);
            System.out.println("The second by length string in array is " + arrayOfString[1]);
            System.out.println("Repeat one more time. If not - press N");
            if ("n".equals(InputString.readLine().toLowerCase())) {
                isExit = true;
            }
        }
    }
}